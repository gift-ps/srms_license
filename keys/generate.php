<?php
  $super_db_server = "localhost";
  $super_db_user = "root";
  $super_db_pass = "";
  $super_db_name = "srms_keys";
  $super_db = mysqli_connect($super_db_server, $super_db_user, $super_db_pass, $super_db_name);
?>

<?php 
  
  if(!empty($_POST['generate_key'])){
      $name = $_POST['name'];
      $city = $_POST['city'];
      $phone = $_POST['phone'];
      $type = $_POST['type'];
      $users = $_POST['users'];

      $date = date('Y-m-d');
      $year = date('Y');
      
      $name_code = abs(crc32($name));
      $city_code = abs(crc32($city));
      $phone_code = abs(crc32($phone));
      $date_code = abs(crc32($date));
      $year = 2021;                               

      $_key = substr($name_code, 0, 4)."-".substr($city_code, 0, 4)."-".substr($phone_code, 0, 4)."-".substr($date_code, 0, 4)."-".substr($year, 0, 4);
      // echo $_key;


      $sql = "INSERT INTO _keys (`name`, `city`, `phone`, `_key`, `date`, `duration`,`users`)
                          VALUES('$name','$city','$phone','$_key','$date', '$type', '$users')";
      
      $success = mysqli_query($super_db, $sql)or die('Could not enter data: '.mysqli_error($super_db)) or die('Could not insert data: '.mysqli_error($super_db));
      $_key_id = mysqli_insert_id($super_db);

      if($success){
        $licence_key = $_key;
        $licence_msg = "A new key for <b>$name</b> has been generated successfully. <br/>";
      }

  }

?>
<!DOCTYPE html>
<html lang="en" >
<head>
  <meta charset="UTF-8">
  <title>Chesco School and LMS Super-Admin</title>
  <link rel="stylesheet" href="../assets/style.css">

  <style>
    .users{
      background-color: #f6f6f6;
      border: none;
      color: #0d0d0d;
      padding: 15px 32px;
      text-align: center;
      text-decoration: none;
      display: inline-block;
      font-size: 16px;
      margin: 5px;
      width: 85%;
      border: 2px solid #f6f6f6;
      -webkit-transition: all 0.5s ease-in-out;
      -moz-transition: all 0.5s ease-in-out;
      -ms-transition: all 0.5s ease-in-out;
      -o-transition: all 0.5s ease-in-out;
      transition: all 0.5s ease-in-out;
      -webkit-border-radius: 5px 5px 5px 5px;
      border-radius: 5px 5px 5px 5px;
    }
  </style>

</head>
<body>
<!-- partial:index.partial.html -->
<div class="wrapper fadeInDown">
  
  <h3 style="color:#9e9e9e">Generate New License Key</h3>
  
  
  <div id="formContent">
      <br>
      <form  method="post" action="#">      
        
        <!-- <label class="" for="name"> Name </label> -->
        <input type="text" id="name" class="fadeIn second" name="name" placeholder="School Name" required />

        <!-- <label class="" for="city"> City </label> -->
        <input type="text" id="city" class="fadeIn third" name="city" placeholder="City" required />
        
        <!-- <label class="" for="phone"> Phone </label> -->
        <input type="text" id="phone" class="fadeIn third" name="phone" placeholder="Phone" required />

        <!-- <label class="" for="users"> Number of users </label> -->
        <div class="">
          <input type="number" id="users" class="fadeIn second users" name="users" placeholder="No. of Users" required />
        </div>

        <select name="type" id="type" type="text" class="fadeIn second" style="text-align:center;">
          <option value="30">One Month Trial </option>
          <option value="60">Two Months Trial </option>
          <option value="365">One Year License </option>
        </select>

        <input type="submit" class="fadeIn " name="generate_key" value="Generate">
      </form>

  </div>

  <br/>
  <?php
    if(isset($success)){
        echo "<p style='color:green'>".$licence_msg."</p>";
        echo "New Key: <br/>
              <b style='background-color: white; padding:10px; border-radius:5px' id='key'>".$licence_key."</b><br/>";
    }
  ?>

</div>

  
</body>
</html>
